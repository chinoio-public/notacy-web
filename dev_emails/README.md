This folder is used for development.

With the default settings, the emails that are sent by the application will be
stored here instead.

Change the EMAIL_URL in your environment to use an actual email service, or 
[check the main README.md](../README.md#customize) to learn more.
