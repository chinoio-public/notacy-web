"""
This settings file overrides core settings defined in settings/default.py
that are useful for local development.
"""
from .default import *

# If DEBUG is not defined explicitly, set it to True by default
DEBUG = env.bool('DEBUG', True)
ALLOWED_HOSTS += ['localhost', '127.0.0.1', '0.0.0.0', '[::1]']

# Don't use production API
CHINO_HOST = 'https://api.test.chino.io/'
