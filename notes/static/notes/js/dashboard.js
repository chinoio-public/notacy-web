/*
 Copyright (c) 2020 Chino.io
*/

( function ( $ ) {
    $(document).ready(function () {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        // resize_panel(".same-height", ".card-title");
        resize_panel(".same-height", ".card");
    });
} )( jQuery );

function v(baseUrl) {
    jQuery(".card-header").click(function (e) {
        e.preventDefault();
        let t = e.target;
        if (!(jQuery(t).hasClass('btn') || jQuery(t).hasClass('fa'))) {
            e.stopImmediatePropagation();
            jQuery(location).attr("href", baseUrl.replace("00000000-0000-0000-0000-000000000000", jQuery(this).data("id")));
        }
    });
}

function resize_panel(selector, what) {
    var objs = jQuery(selector).find(what);
    var o_h = 0;
    objs.each(function () {
        if (o_h < jQuery(this).height()) {
            o_h = jQuery(this).height();
        }
    });
    objs.each(function () {
        jQuery(this).height(o_h);
    });
}
