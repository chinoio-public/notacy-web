$.noConflict();

jQuery(document).ready(function($) {

    "use strict";

    [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
        new SelectFx(el);
    } );

    jQuery('.selectpicker').selectpicker;


    $('#menuToggle').on('click', function() {
        $('body').toggleClass('open');
    });

    $('.search-trigger').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        // $('.search-trigger').parent('.header-left').addClass('open');
        $('#search-query').parent('.header-left').addClass('open');
    });

    $('.search-close').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        // $('.search-trigger').parent('.header-left').removeClass('open');
        $('#search-query').parent('.header-left').removeClass('open');
    });

    $('.trigger-async').click(function () {
        $(this).text($(this).data('text-async'));
    });

    $('.note-badge').each(function () {
        $(this).text(
            $(this).data("tag").length <= 10 ? $(this).data("tag") : $(this).data("tag").substring(0, 10) + '…'
        );
    });

    $("#tag-create").click(function(e){
        e.preventDefault();
        let f = $("#new-tag");
        $.post(f.attr('action'), $(f).serialize(), function () {
            location.reload();
        });
    });

    $(".delete-tag").each(function(){
        let f = $(this);
        $(f).find('[type="submit"]').click(function(e) {
            e.preventDefault();
            $.post(f.attr('action'), $(f).serialize(), function () {
                location.reload();
            });
        });
    });

    $('.show-tooltip').tooltip({
        delay: { "show": 350, "hide": 100 }
    });
});

function d(deleteUrl, token) {
    jQuery('.confirm-del').click(function (e) {
        e.preventDefault();
        jQuery.post(deleteUrl,
            {
                "document_id": jQuery(this).data("id"),
                "csrfmiddlewaretoken": token
            },
            function () {
                location.reload();
            }
        );
    })
}