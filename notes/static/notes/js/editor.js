/*
 Copyright (c) 2020 Chino.io
*/

jQuery(document).ready(function($) {
    let savedContent = $("#id_content").val();
    let saved = false;

    $(".save-status-icon").show().delay(3 * 1000).fadeOut();

    $('#id_content').on( 'change keydown keyup ', function (){
        $('#id_content').height(0).height(this.scrollHeight * 1.05);
    }).change();

    $('.no-labels').find('label').each(function (i, val) {
        $(val).hide();
    });

    let tags = $('#taglist');
    tags.find(".form-check").addClass('form-check-inline');
    tags.find('.form-check-label').each(function (e, item) {
        // get tag name and hide checkbox
        let input = $(item).find('input');
        let tag = $(input).attr('value');
        input.hide();
        // prepend related icon to the label
        let tagIcon = getIcon(tag);
        $(item).prepend(tagIcon);
        $(tagIcon).show();
        // add select effect
        $(item).change(function () {
            if ($(input).prop('checked'))
                $(this).addClass('checked');
            else
                $(this).removeClass('checked');
        });
        $(item).change(); // apply effect to initialized items
    });

    $('.save-button').click(function (e) {
        e.preventDefault();
        savedContent = $("#id_content").val();
        $('#editable-note').submit();
    });
    $(".save-and-close").click(function (e) {
        e.preventDefault();
        $('#save-mode').attr('value', 'close');
        savedContent = $("#id_content").val();
        $('#editable-note').submit();
    });

    function notSaved() {
        return $("#id_content").val() !== savedContent;
    }
    window.onbeforeunload = function() {
        if (notSaved()) {
            return "You have unsaved changes. Do you want to leave?";
        } else {
            return;
        }
    };
});

function getIcon(tag) {
    let $ = jQuery;
    let iconSet = $("#hidden-icons").children();
    let icon = undefined;
    $(iconSet).each(function () {
        if ($(this).attr("title") === tag) {
            icon = icon || this;
        }
    });
    return icon;
}