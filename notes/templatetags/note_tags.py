# Copyright (c) 2020 Chino.io
"""
Template tags that are used to render Notacy tags on page
"""
import hashlib

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

styles = ["primary", "secondary", "success", "danger", "warning", "info",
          "primary", "secondary", "success", "danger", "warning", "info", "dark"]


@register.filter(name="tagstyle")
@stringfilter
def get_style(txt: str):
    """
    return a style for a tag, computed according to the SHA1 hash of the tag name

    :param txt:
    :return:
    """
    i = _h(txt) * 3.1415 % len(styles)
    i = int(i)
    i *= len(txt)
    # normalize into array's boundaries
    i %= len(styles)

    return styles[i]


def _h(txt):
    return int(
        hashlib.sha256(str(txt).encode('utf-16')
                       ).hexdigest(),
        16
    )
