# Copyright (c) 2020 Chino.io
from django import template

from notes.utils import ParseUtils

register = template.Library()


@register.filter(name='get', is_safe=True)
def get_param(resource, attribute_name: str):
    """
    Template filter which extracts an attribute from a dictionary

    :param resource: either a (dict) or a Chino.io resource
    :param attribute_name: (str) the name of the attribute to be extracted. Allows nested attributes:
                            e.g. {'out': {'in' : ['a', 'b']}} -> "out.in"

    :return: the value of the specified attribute in the resource
    """
    if not isinstance(resource, dict):
        resource = resource.to_dict()
    return _get_param(resource, attribute_name.split("."))


def _get_param(resource, attribute_path):
    if not attribute_path:
        return resource
    else:
        res = resource[attribute_path[0]]
        path = attribute_path[1:]
        return _get_param(res, path)


@register.filter(name='str2date')
def str2date(date_str: str):
    return ParseUtils.get_datetime(date_str)
