# Copyright (c) 2020 Chino.io
import logging

import django.contrib.auth.views as auth
from chino.exceptions import CallError
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.http import urlsafe_base64_decode
from django.views import generic

from notes import forms
from notes.backends import ChinoAuthBackend
from notes.utils import ParseUtils, LoginTokenManager, AuthUtils

log = logging.getLogger("notes.view")
APP_NAME = "Notacy"


# # # Notes # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class DocumentListView(generic.ListView):
    """Displays a list of Notes.

    Reads a list of Chino.io Documents and passes them to the template.
    """
    template_name = 'notes/dashboard.html'
    allow_empty = True

    def get_queryset(self):
        # get the API client for the current user
        api_client = self.request.chino
        # get the Schema that contains the notes of the current user
        schema = self.request.user.attributes.schema_id
        # return the list of notes
        response = api_client.documents.list(schema, full_document=True)
        return response.documents

    def get_context_data(self, *, object_list=None, **kwargs):
        # add context to every view
        return super().get_context_data(
            app_name=APP_NAME, page_name="Dashboard",
            tag_list=self.request.user.attributes.all_tags,
            **kwargs
        )


class DeleteNoteView(generic.RedirectView):
    """Deletes a Note (POST only)
    """

    def post(self, request, *args, **kwargs):
        """
        Delete a Note
        """
        # read document ID from URL
        doc_id = request.POST['document_id']
        try:
            # delete Document from Chino.io
            self.request.chino.documents.delete(doc_id, force=True)
        except CallError as ce:
            log.warning(ce.message)
        finally:
            # regardless of the outcome, redirect to dashboard
            return redirect('dashboard')


class EditNoteView(generic.FormView):
    """Shows an Editor page to create or update a Note
    """
    template_name = "notes/editor.html"
    form_class = forms.NoteForm

    page_name = "New Note"
    title = ""  # gets read from Chino.io in get() and post()
    save_mode = "stay"
    doc_id = None
    success = False

    def get_initial(self):
        initial = {"tags": []}
        if self.doc_id:
            document = self.request.chino.documents.detail(self.doc_id)
            self.title = document.content.title
            initial["title"] = self.title
            initial["content"] = document.content.content
            initial["tags"] = document.content.tags
        return initial

    def get_form(self, form_class=None):
        form = super().get_form(self.form_class)
        # Display tags: pass each tag as a tuple (html_id, text). In our
        # implementation, the HTML id has the same value as the tag text.
        user_tags = self.request.user.attributes.all_tags
        displayed_tags = []
        for tag_name in user_tags:
            displayed_tags.append((tag_name, tag_name))
        form.fields['tags'].choices = displayed_tags
        return form

    def get_context_data(self, **kwargs):
        """
        Provide context to the Editor template and render with TemplateView
        """
        d = dict(app_name=APP_NAME,
                 page_name=(
                     "%s" % self.title if self.title else "%s" % self.page_name),
                 success=self.success or False,
                 document_id=self.doc_id or None,
                 all_tags=self.request.user.attributes.all_tags,
                 **super().get_context_data(**kwargs))
        return d

    def get(self, request, *args, **kwargs):
        """
        Get a new Editor page for creating or updating a Note.
        The Note's ID is in the URL
        """
        # remove "success" flag from session, will be initialized in form_valid
        self.request.session.pop("success", False)
        self.doc_id = kwargs.get('document_id', None)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Read user input and create/update a Note on Chino.io
        """
        self.doc_id = kwargs.get('document_id', None)
        self.save_mode = request.POST.get("savemode", self.save_mode)
        # Display error/success msg
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        """
        Update the Note on Chino.io with the new content

        :param form: the validated and cleaned form

        :return: a HTTP response
        """
        data = form.to_json()
        try:
            if self.doc_id:
                # update page title with the new Note's name
                self.page_name = "Edit"
                self.title = data["title"]
                # read document ID from URL
                doc_id = str(self.doc_id)
                # update note on Chino.io
                self.request.chino.documents.update(doc_id, content=data)
                # display success msg
                messages.success(self.request, "note updated!")
                self.request.session["success"] = True
            else:
                # create new document on Chino.io
                user = self.request.user
                self.doc_id = self.request.chino.documents.create(user.attributes.schema_id,
                                                                  content=data,
                                                                  consistent=True).document_id
                self.request.session["success"] = True
        except CallError as ce:
            log.exception("* * * Chino API Error:")
            self.request.session["success"] = False
            # show error
            messages.error(self.request, "Failed to save Note (502)")
            self.request.session["success"] = False
        finally:
            if self.save_mode == "stay" and self.doc_id:
                # show Editor with messages
                return redirect('edit', document_id=self.doc_id)
            else:
                # the flag is only needed for rendering the 'editor' template
                self.request.session.pop("success")
                return redirect('dashboard')

    def form_invalid(self, form):
        """
        Send back a response with errors

        :param form:
        :return:
        """
        self.request.session["success"] = False
        resp = super().form_invalid(form)
        resp.status_code = 400
        return resp


# # # User profile # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class UserDetailsView(generic.TemplateView):
    """Serves and manages the user's profile page
    """
    template_name = 'notes/profile.html'

    page_name = "Your profile"

    def get_context_data(self, **kwargs):
        usr = self.request.user
        fname = usr.attributes.first_name
        lname = usr.attributes.last_name
        return super().get_context_data(
            app_name=APP_NAME,
            user=usr,
            firstname=fname,
            lastname=lname,
            created=ParseUtils.get_datetime(usr.insert_date),
            profile_img='img/user.png',
            page_name=self.page_name,
            **kwargs)


class UserUpdateView(auth.PasswordChangeView):
    """Start the password update workflow
    """
    template_name = "notes/auth/psw_change.html"
    page_name = "Change password"
    form_class = forms.NotacyPasswordChangeForm

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid = False

    def get_context_data(self, **kwargs):
        return super().get_context_data(
            app_name=APP_NAME,
            user=self.request.user,
            valid=self.valid,
            page_name=self.page_name,
            main_txt="Insert a new password:",
            **kwargs)

    def form_valid(self, form):
        user = form.save()
        self.request.session["user"] = ParseUtils.get_serializable(user)
        self.valid = True
        messages.success(self.request, "Password changed!")
        return redirect('user')


class UserDeleteView(generic.RedirectView):
    """Delete a user's account.

    This feature is accessible from the User Info page.
    """

    def get_redirect_url(self, *args, **kwargs):
        return reverse('user')

    def post(self, request, *args, **kwargs):
        """
        Delete all the Notes and the Notes Schema for the deleted user.

        :param request: the HTTP request
        :return: a redirect response to the /logout page
        """
        u = self.request.user
        client = AuthUtils.get_client(auth="customer")
        # delete all user's Notes
        notes = client.documents.list(u.attributes.schema_id).documents
        while len(notes) > 0:
            for note in notes:
                client.documents.delete()
            notes = client.documents.list(u.attributes.schema_id).documents
        # delete the user's Notes Schema
        client.schemas.delete()
        # delete user's profile
        client.users.delete()
        # invalidate session and return to login page
        messages.success(request, f"Account {u.username} has been deleted.")
        return redirect('logout')


# # # Tags # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class ManageTagsView(generic.TemplateView):
    """Creates tags
    """
    template_name = 'notes/tags.html'
    MAX_TAGS = 10

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self._target_tag = None

    def dispatch(self, request, *args, **kwargs):
        self.user = self.request.user
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        return dict(
            MAX_TAGS=self.MAX_TAGS,
            app_name=APP_NAME,
            page_name='Your tags',
            user=self.user,
            tags=self.user.attributes.all_tags,
            **super().get_context_data(**kwargs)
        )

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self._target_tag = self.request.POST.get('tag', None)

        error = self.is_invalid(self._target_tag, self.user.attributes.all_tags)
        if not error:
            self.user.attributes.all_tags = self.update_taglist(self.user.attributes.all_tags, self._target_tag)
            self._update_user(request, AuthUtils.get_client(auth="customer"))
        return redirect('tags')

    def is_invalid(self, tag, existing_tags):
        if not tag:
            messages.error(self.request, 'Please insert a valid tag name')
            return True
        elif self._target_tag in self.user.attributes.all_tags:
            messages.error(self.request, 'tag "%s" already exists' % tag)
            return True
        elif len(self.user.attributes.all_tags) >= self.MAX_TAGS:
            messages.error(self.request, 'You can not have more than %i tags' % self.MAX_TAGS)
            return True
        else:
            return False

    def update_taglist(self, tags: list, target_tag):
        tags.append(target_tag)
        return tags

    def _update_user(self, request, client):
        request.user = self.user = client.users.partial_update(self.user.user_id,
                                                               attributes={"all_tags": self.user.attributes.all_tags})
        request.session['update_user'] = True


class DeleteTagView(ManageTagsView):
    """Deletes tags
    """

    def post(self, request, *args, **kwargs):
        try:
            super().post(request, *args, **kwargs)
        except Exception as e:
            log.error(e)
            raise e
        finally:
            if messages.get_messages(request) is []:
                messages.info(request, 'Tag "%s" deleted.')
        return redirect('tags')

    def update_taglist(self, tags: list, target_tag):
        tags.remove(target_tag)
        return tags

    def is_invalid(self, tag, existing_tags):
        if not tag or self._target_tag not in existing_tags:
            messages.error(self.request, 'Please insert a valid tag name')
            return True
        else:
            return False

    def _update_user(self, request, client):
        super()._update_user(request, client)
        tagged_docs = client.searches.documents(
            request.user.attributes.schema_id,
            filters=[
                {
                    "field": "tags",
                    "type": "eq",
                    "value": self._target_tag
                }
            ]).documents
        for doc in tagged_docs:
            c = doc.content
            c.tags.remove(self._target_tag)
            client.documents.update(doc.document_id, content=c.to_dict())


# # # Auth # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class LoginView(generic.FormView):
    """Authenticates a user
    """
    form_class = forms.LoginForm
    username = None
    template_name = "notes/login.html"
    show_register = True  # Show "Register" button (overridden in subclass RegisterView)
    page_name = "Login"
    action = "Login"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.username:
            context["username"] = self.username
        return dict(show_register=self.show_register,
                    app_name=APP_NAME, page_name=self.page_name,
                    hide_nav=True,
                    action=self.action,
                    **context)

    def get_initial(self):
        # if the username is stored in session, write it in the form
        username = self.request.session.pop("username", None)
        if not username:
            return {}
        return {
            "username": username,
            "password": ""
        }

    def form_valid(self, form):
        """
        Validate login credentials with Chino.io

        :param form: the cleaned and validated LoginForm
        :return: a HTTP response with the outcome of the operation
        """
        username, password = form.cleaned_data["username"], form.cleaned_data["password"]
        user = ChinoAuthBackend().authenticate(username=username, password=password)
        if not user:
            messages.error(self.request, "Invalid username or password")
            resp = self.render_to_response(context=self.get_context_data(form=form))
            resp.status_code = 401
        else:
            log.debug(f"User {username} logged in.")
            self.request.session["user"] = ParseUtils.get_serializable(user)
            resp = redirect('dashboard')
        return resp

    def form_invalid(self, form):
        """
        Return a 401 Unauthorized response

        :param form: the cleaned form
        """
        resp = super().form_invalid(form)
        resp.status_code = 401
        return resp


class RegisterView(LoginView):
    """Register users

    Whenever a new user is registered:
      - a Schema which will hold the user's Notes
      - a Chino.io User with the new user's personal information and a
        reference to the Schema
      - Permissions are granted to the new User over its Schema
    """
    form_class = forms.RegisterForm

    show_register = False
    page_name = "New account"

    action = "Register"

    def form_invalid(self, form):
        resp = super().form_invalid(form)
        resp.status_code = 400
        return resp

    def form_valid(self, form):
        schema_id = None
        user_id = None
        form_data = form.cleaned_data
        # read User data from the form
        user_attributes = {
            'first_name': form_data['first_name'],
            'last_name': form_data['last_name'],
            'all_tags': []
        }
        notes_schema_fields = [
            {"name": "title", "type": "string", "indexed": True},
            {"name": "content", "type": "text", "indexed": False},
            {"name": "tags", "type": "array[string]", "indexed": True}
        ]
        try:
            # Create Notes Schema for the User
            schema_id = AuthUtils.get_client(auth="customer").schemas.create(
                repository=settings.REPOSITORY_ID,
                description=f"{form_data['username']} ~ Notes",
                fields=notes_schema_fields
            ).schema_id
            # store Schema ID in the user's attributes
            user_attributes['schema_id'] = schema_id
            # Create new User
            user_id = AuthUtils.get_client(auth="customer").users.create(
                user_schema_id=settings.USER_SCHEMA_ID,
                username=form_data['username'], password=form_data['password'],
                attributes=user_attributes,
                consistent=True
            ).user_id
            # grant Permissions to create, read, delete, update and list all documents (Notes)
            # that will ever be created in the user's Notes Schema.
            AuthUtils.get_client(auth="customer").permissions.resource_children(
                action="grant", resource_type="schemas", resource_id=schema_id,
                resource_child_type="documents", subject_type="users", subject_id=user_id,
                manage=['C', 'R', 'U', 'D', "L"])
            # return login form initialized with username
            self.username = self.request.session["username"] = form_data['username']
            messages.success(self.request, "Account created! Log in to start using Notacy.")
            return redirect("login")
        except CallError as err:
            # Cleanup the resources that were created before the Exception
            try:
                if schema_id:
                    AuthUtils.get_client(auth="customer").schemas.delete(schema_id)
                if user_id:
                    AuthUtils.get_client(auth="customer").users.delete(user_id)
            finally:
                # Show error msg on page
                messages.error(self.request, str(err.code) + " ~ " + str(err.message))

        return super().render_to_response(context=self.get_context_data())


class LogoutView(generic.RedirectView):
    """Logout a user
    """
    def get(self, request, *args, **kwargs):
        # delete session data and redirect
        request.session.flush()
        request.user = None
        request.client = None
        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        """
        Redirect to login page
        """
        destination = reverse('login')
        messages.success(self.request, "Logout successful")
        return destination


# # # Password recovery # # # # # # # # # # # # # # # # # # # # # # # # # # #

class NotacyPasswordResetView(auth.PasswordResetView):
    """Customizes Django default PasswordResetView
    """
    template_name = 'notes/auth/psw_reset.html'
    form_class = forms.NotacyPasswordResetForm

    content_type = 'text/html; charset="UTF-8"'
    html_email_template_name = 'notes/auth/psw_reset_email.html'
    email_template_name = html_email_template_name
    subject_template_name = 'notes/auth/psw_reset_email_subject.txt'

    from_email = APP_NAME.lower() + "_noreply@chino.io"

    # Pass custom values to the template engine
    extra_context = dict(
        app_name=APP_NAME,
        hide_nav=True,
        page_name="Reset password (1/2)",
        main_txt="Forgot your password?",
        secondary_txt="Type in your email and we will send you an email to reset your password."
    )


class NotacyPasswordResetDoneView(auth.PasswordResetDoneView):
    """Customizes Django default PasswordResetDoneView
    """
    template_name = 'notes/auth/psw_reset_done.html'

    def get_context_data(self, **kwargs):
        """
        Pass custom page title to the template engine
        """
        return dict(
            app_name=APP_NAME,
            hide_nav=True,
            page_name="Reset password (2/2)",
            **super().get_context_data(**kwargs)
        )


class NotacyPasswordResetConfirmView(auth.PasswordResetConfirmView):
    """Customizes Django default PasswordResetConfirmView
    """
    template_name = 'notes/auth/psw_reset.html'
    form_class = forms.NotacyPasswordSetForm

    def get_context_data(self, **kwargs):
        """
        Pass custom values to the template engine
        """
        return dict(
            app_name=APP_NAME,
            hide_nav=True,
            page_name="Change password",
            main_txt="Choose a new password",
            secondary_txt="You won't be able to use the old password again.",
            valid=self.validlink,
            main_err_txt="Ooops! You have no right to be here.",
            secondary_err_txt="The link may be expired, or you may not have the authorization to access this page.",
            **super().get_context_data(**kwargs)
        )

    def get_user(self, uidb64):
        """Read user from Chino.io

        Wraps the user with custom attributes so that it can be read by
        Django password recovery tools.

        :param uidb64: the Chino.io User ID represented as a urlsafe,
                       base64-encoded UUID

        :return: the user with additional attributes, or None in case of errors
        """
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = AuthUtils.get_client(auth="customer").users.detail(uid)
            self.request.session["username"] = user.username
            # wrap user
            return LoginTokenManager._wrap_user_for_token(user)
        except Exception as e:
            log.exception(e)
            return None


class NotacyPasswordResetCompleteView(generic.RedirectView):
    """Completes the password reset workflow.

    Redirects to login page.
    """
    url = '/'
