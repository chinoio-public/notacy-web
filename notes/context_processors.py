# Copyright (c) 2020 Chino.io
from notes import views


def user_tags(request):
    ctx =  dict(MAX_TAGS=views.ManageTagsView.MAX_TAGS,
                all_tags=[])
    if hasattr(request, 'user'):
        ctx["all_tags"] = request.user.attributes.all_tags
    return ctx
