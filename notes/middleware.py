# Copyright (c) 2020 Chino.io
from datetime import datetime

from chino.exceptions import CallError
from chino.objects import User
from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import resolve

from notes.utils import ParseUtils, AuthUtils


class ChinoAuthMiddleware:
    """
    Handles all authentication operations against Chino.io
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, request, view, view_args, view_kwargs):
        """
        Check that the currently logged usr has permission to see the requested view

        :param request: the HTTP request
        :param view: unused - required by Django middleware
        :param view_args: unused - required by Django middleware
        :param view_kwargs: unused - required by Django middleware

        :return: a redirect to the login screen if the user is not authorized, otherwise `None`
        """
        # check if the view can be accessed by non-authenticated users
        if resolve(request.path).view_name in settings.ALLOWED_VIEWS:
            return None
        # check if user information is stored in session
        user = request.session.get("user", None)
        if not request.session.session_key or not user:
            messages.error(request, "401 ~ User not authenticated")
            return redirect('login')
        # check whether the access_token needs to be refreshed
        try:
            client = self._token_refresh(user["tokens"])
        except CallError:
            # if token refresh failed, force user to login
            request.session.flush()
            messages.error(request, "Session expired. Please login again")
            return redirect('login')
        # update user if required
        if request.session.get("update_user", False):
            user = AuthUtils.refresh_user(client, request)
            request.session["update_user"] = False
        # add params to request
        # # add user info
        request.user = User(**user)
        request.user.is_authenticated = user["is_authenticated"]
        # # add API client for the logged user
        request.chino = client
        # continue processing view
        return None

    @staticmethod
    def process_exception(request, exception):
        """
        Handle authentication errors

        :param request:
        :param exception:
        :return:
        """
        auth_errors = {
            "Authorization Required": {"code": 401, "message": "Please log in again"},
            "Permission denied, not authorized": {"code": 403, "message": "Permission denied"},
        }
        if isinstance(exception, CallError) and exception.message in auth_errors.keys():
            try:
                client = getattr(request, "chino", None)
                if client:
                    client.users.refresh(client.auth.refresh_token)
                    request.user = client.users.current()
                    request.chino = client
                    return redirect(request.path)
            except CallError as nested_exception:
                http_err = auth_errors.get(nested_exception.message, {
                    "code": 500, "message": "Internal server error"
                })
                request.session.flush()
                messages.error(request, str(http_err["code"]) + " ~ " + http_err["message"])
                return redirect('login')
        else:
            return None

    def _token_refresh(self, tokens):
        """
        Check if the access tokens expire in less than 5 minutes.
        If this is the case, Chino.io API are called in order to refresh the tokens.

        `tokens` must contain at least the following keys:
            - `"access_token"`
            - `"refresh_token"`
            - `"expiry"`

        :param tokens: a (dict) containing the access tokens

        :return: an authenticated Chino API client
        """
        client = AuthUtils.get_client(auth=tokens)
        try:
            # compare timezone-aware dates in the same format
            expiry_time = ParseUtils.get_datetime(tokens['expiry'])
            now = ParseUtils.get_datetime(str(datetime.now()))
            delta = expiry_time - now
            # compute
            mins, secs = divmod(delta.days * 86400 + delta.seconds, 60)

        except TypeError as te:
            # raise CallError(401, "No expiry date - session is invalid")
            mins = -1

        if mins <= 4:
            client.users.refresh(client.auth.refresh_token)
        return client

