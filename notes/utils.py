# Copyright (c) 2020 Chino.io
from chino.api import ChinoAPIClient
from chino.objects import User
from django.conf import settings
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, make_aware

from notes.backends import ChinoAuthBackend


class ParseUtils:
    """
    Contains utility methods for transformation of data to be used in session management and web templates
    """
    @staticmethod
    def get_serializable(o):
        """
        Makes the provided object serializable

        :param o: a object with a to_dict() method

        :return: a (dict) containing the object's content, converted to (str)
        """
        d = o.to_dict()
        d["tokens"]["expiry"] = str(d["tokens"]["expiry"])
        return d

    @staticmethod
    def get_datetime(date_str):
        """
        Turn the provided date string to a format that is timezone-aware
        and that can be parsed by Django template engine.

        :param date_str: a string containing a date in an ISO format
        :return: a timezone-aware date that can be passed to a template context
        """
        if date_str is None or date_str == 'None':
            return None
        ret = parse_datetime(date_str)
        return ret if is_aware(ret) else make_aware(ret)


class AuthUtils:
    @staticmethod
    def get_client(auth):
        """
        Get a ChinoAPIClient to perform API calls to Chino.io API

        :param auth: either the (str) "customer", to get a customer client (no
                     restrictions or UAC), or a (dict) containing an
                     "access_token" and a "refresh_token" of a logged user

        :return: a (ChinoAPIClient) with the specified auth method
        """
        if auth is "customer":
            client = ChinoAPIClient(
                settings.CHINO['CUSTOMER_ID'],
                settings.CHINO['CUSTOMER_KEY'],
                url=settings.CHINO['HOST']
            )
            client.auth.set_auth_admin()
        else:
            client = ChinoAPIClient(
                settings.CHINO['CUSTOMER_ID'],
                bearer_token=auth["access_token"],
                client_id=settings.CHINO["APP_ID"],
                client_secret=settings.CHINO["APP_SECRET"],
                url=settings.CHINO['HOST']
            )
            client.auth.refresh_token = auth["refresh_token"]
            client.auth.bearer_exp = auth["expiry"]

            client.auth.set_auth_user()
        return client

    @staticmethod
    def refresh_user(client, req):
        user = ChinoAuthBackend().authenticate(
            access_token=client.auth.bearer_token,
            refresh_token=client.auth.refresh_token)
        req.session["user"] = ParseUtils.get_serializable(user)
        return req.session["user"]


class LoginTokenManager:

    @staticmethod
    def _wrap_user_for_token(user: User, password=""):
        """
        Adds aliases for some of the user's attributes, so that Django can
        generate a valid password reset token.
        This method will add the required aliases and initialize them as
        follows:
            - `pk` is set to the value of `user_id`;
            - `password` is set to the provided value. This value is not used,
              since the user auth is handled with the custom backend
            - `last_login` is set to None, so that the value is ignored by the
              hash function.

        :return: the user with the `pk`, `password` and `last_login` attributes.
        """
        setattr(user, "pk", user.user_id)
        setattr(user, "last_login", ParseUtils.get_datetime(user.last_update))
        setattr(user, "password", password)
        return user

