# Copyright (c) 2020 Chino.io
from django.apps import AppConfig


class NotesConfig(AppConfig):
    name = 'notes'
