import random
import os

import environ
from chino import api as chino_api
from django.conf import settings
from django.core.management import BaseCommand

USER_DESCRIPTION = "Notacy users"
REPOSITORY_DESCRIPTION = "Notacy main repository"
APP_NAME = "Notacy backend"
BASE_DIR = environ.Path(__file__) - 4


class Command(BaseCommand):
    """Set up your Chino.io account to use Notacy.
    """
    verbose = 1
    env_data = dict()

    def add_arguments(self, parser):
        parser.usage = """
    manage.py setupchino [-h] [--host HOST] [--env ENV] [--delete-app]
                         [--django-key] [--version] [-v {0,1,2,3}]
                         [--settings SETTINGS] [--pythonpath PYTHONPATH]
                         [--traceback] [--no-color] [--force-color]

    Scans your Chino.io account looking for the resources that are required to 
    run the Notacy app - a Repository, an Application and a UserSchema - then 
    writes in your .env file the IDs of those resources. If the required 
    resources don't exist, they will be created.
    
    Before executing this command, write your CUSTOMER_ID and CUSTOMER_KEY in
    the .env file. Nothing will be deleted from your Chino.io account.
    
    You will need a valid APP_ID and APP_SECRET pair. If you don't have it,
    use flag --delete-app to create the Notacy APP_ID and APP_SECRET pair 
    (a.k.a. Application) on Chino.io. WARNING: This will delete the previous
    Notacy Application, if it exists on your account.
    """
        # Chino API host
        parser.add_argument(
            '--host', type=str, default="http://api.test.chino.io",
            help='Set the host of the Chino.io API. '
                 'Default: "http://api.test.chino.io"')
        # Environment (.env) file to update
        parser.add_argument(
            '--env', type=str, default="settings/.env",
            help="Specify the path to a .env file where the setup "
                 "configuration will be stored. Default: a file called '.env' "
                 "in the root of this project.")
        # --delete-app
        parser.add_argument(
            '--delete-app', action='store_true',
            help="This cmd by default deletes and re-creates an "
                 "Application on Chino in order to read its APP_SECRET. "
                 "Without this flag nothing is deleted from the account. You "
                 "can omit it if you already know the APP_SECRET")
        # Generate Django SECRET_KEY
        parser.add_argument(
            '--django-key', action='store_true',
            help="Generate a new random Django SECRET_KEY to store in the .env"
                 " file")

    def handle(self, *args, **options):
        self.verbose = options['verbosity']
        # This message is printed only when settings/defaults.py finds some
        # undefined variables and prints WARNINGS about it.
        if getattr(settings, 'CHINO_SETUP_INCOMPLETE', False):
            print("NOTE: all the missing variables will be added now.")
            print()
        # Load .env file
        env_file = os.path.abspath(options['env'])
        env = environ.Env(CUSTOMER_ID=str, CUSTOMER_KEY=str)
        try:
            env.read_env(env_file=BASE_DIR.file(env_file))
        except:
            raise Exception(f"Failed to load env file: {BASE_DIR.path(env_file)}")
        # Check if customer Id/Key are present in the .env file
        try:
            customer_id, customer_key = self._read_credentials(env_file)
            if not customer_id or not customer_key:
                # if None or empty, we want to stop here
                raise Exception()
            self._print_verbose(3, "Found CUSTOMER_ID and CUSTOMER_KEY")
        except:
            msg = "Before executing this command, write your Chino "
            msg += "CUSTOMER_ID and CUSTOMER_KEY in the .env file:\n"
            msg += f" {os.path.abspath(env_file)} \n"
            msg += "If you don't have the credentials, register an account at "
            msg += "https://console.test.chino.io/sign-up"
            # print this msg regardless of the verbosity level
            self._print_verbose(0, msg)
            exit(1)

        # # generate SECRET_KEY
        django_secret_key = self._generate_key(options)
        if django_secret_key:
            self._print_verbose(2, "Generated new Django secret key")
            self._add_to_env('DJANGO_KEY', django_secret_key)
        else:
            self._print_verbose(3, "Skipping Django secret key generation.")
        # Read properties from the input parameters
        host = options['host']
        self._add_to_env('HOST', host)
        client = chino_api.ChinoAPIClient(customer_id, customer_key, url=host)

        # # Create UserSchema if not exists
        user_fields = [
            {"name": "first_name", "type": "string", "indexed": True},
            {"name": "last_name", "type": "string", "indexed": True},
            {"name": "all_tags", "type": "array[string]", "indexed": True},
            {"name": "schema_id", "type": "string", "indexed": False}
        ]
        user_schema_id = None
        user_schemas = client.user_schemas.list(**dict(limit=100)).user_schemas
        for u_schema in user_schemas:
            if u_schema.description == USER_DESCRIPTION:
                # it exists
                user_schema_id = u_schema.user_schema_id
                self._print_verbose(3, "A Notacy UserSchema was found on your "
                                       "account.")
                break
        if user_schema_id is None:
            self._print_verbose(3, "Notacy UserSchema not found. Creating "
                                   "new...")
            # it doesn't exist: create a new one
            user_schema_id = client.user_schemas.create(
                USER_DESCRIPTION, user_fields).user_schema_id
        else:
            # it exists. Since new versions of the Notacy app may change the
            # structure of the UserSchema, we update it.
            self._print_verbose(3, 'Updating UserSchema structure...')
            client.user_schemas.update(user_schema_id,
                                       description=USER_DESCRIPTION,
                                       structure=dict(fields=user_fields))
            self._print_verbose(3, 'Done.')
        # Store the UserSchema ID
        self._add_to_env('USER_SCHEMA_ID', user_schema_id)

        # # Create Repository if not exists
        repo_id = None
        ls = client.repositories.list(**dict(limit=100)).repositories
        for repo in ls:
            if repo.description == REPOSITORY_DESCRIPTION:
                # it exists
                repo_id = repo.repository_id
                self._print_verbose(3, "A Notacy Repository was found on your "
                                       "account.")
                break
        if repo_id is None:
            # it doesn't exist: create a new one
            self._print_verbose(3, "Notacy Repository not found. Creating "
                                   "new...")
            repo_id = client.repositories.create(
                REPOSITORY_DESCRIPTION).repository_id
        # Store the Repository ID
        self._add_to_env('REPOSITORY_ID', repo_id)

        # # Create an Application
        ls = client.applications.list(limit=100).applications
        notacy_backend_app = None
        for app in ls:
            if app.app_name == APP_NAME:
                # it exists
                notacy_backend_app = app
                self._print_verbose(3, "A Notacy Application was found on "
                                       "your account.")
                break
        if notacy_backend_app is None:
            # it doesn't exist: create a new one
            self._print_verbose(3, "Notacy Application not found. Creating "
                                   "new...")
            notacy_backend_app = client.applications.create(APP_NAME,
                                                            "password")

        # If the app exists in Chino.io, APP_SECRET can't be read via the API.
        # In this case, in order to have APP_SECRET to use in the .env , we
        # need to destroy and re-create the app.
        # We only do so if the user explicitly opts in (--delete-app).

        deletion_allowed = options.get("delete_app", False)
        if notacy_backend_app.app_secret is None:
            self._print_verbose(1, "")
            if not deletion_allowed:
                notacy_backend_app.app_secret = None  # will not be updated
                msg = f"Make sure the APP_SECRET is written in your "
                msg += f".env file ({env_file}), otherwise you should delete "
                msg += f"and re-create it in order to get a valid APP_SECRET. "
                msg += "To do so, run this command with '--delete-app'"
                self._print_verbose(1, msg)
            else:
                client.applications.delete(notacy_backend_app.app_id)
                notacy_backend_app = client.applications.create(
                    APP_NAME, "password")
                msg = f"An application called '{APP_NAME}' has been deleted "
                msg += "and re-created." + "\n"
                msg += "This is required because Chino.io API don't allow to "
                msg += "read the APP_SECRET of an existing application. \n"
                msg += "If you run the chino setup again, omit the flag "
                msg += "'--delete-app' to avoid deleting the Application."
                self._print_verbose(1, msg)
        # Store the Application ID/secret
        self._add_to_env('APP_ID', notacy_backend_app.app_id)
        self._add_to_env('APP_SECRET', notacy_backend_app.app_secret)
        # Recap
        self._print_verbose(3, "")
        self._print_verbose(3, f"Writing data to file '{env_file}':")
        for k, v in self.env_data.items():
            self._print_verbose(3, f"    {k.upper()}={v}")
        # Write data
        self._write_env(env_file)
        self._print_verbose(2, "")
        self._print_verbose(2, f"Data written to '{env_file}'.")

    def _add_to_env(self, name, val):
        """Cache a key-value pair to be added to the .env file.

        If the value is None, it will be ignored.
        """
        if val:
            self.env_data[name] = val
            self._print_verbose(2, "- Added to env: "
                                   f"{name}={self.env_data[name]}")
        else:
            self._print_verbose(2, f"No value specified for {name}, ignoring")

    def _write_env(self, out_file):
        """Writes to the specified .env file the cached values.

        Values must be added to the cache using self._add_to_env()
        before invoking this method.
        """
        with open(out_file, 'r+') as env_file:
            content = ""
            # read the content of the .env file and create a copy in memory,
            # updated with the values cached in self.env_data
            for line in env_file:
                new_line = line
                if line.startswith('#'):
                    pass
                if line.startswith('DJANGO_KEY'):
                    django_key = self.env_data.get('DJANGO_KEY', None)
                    if django_key:
                        new_line = "DJANGO_KEY=" + django_key + "\n"
                else:
                    for key, value in self.env_data.items():
                        if line.startswith(key):
                            new_line = key + "=" + value + "\n"
                            break
                content += new_line
        # Finally, write the updated content to the same .env file
        with open(out_file, 'w') as env_file:
            env_file.write(content)
            env_file.flush()

    def _generate_key(self, options):
        """Generates and return a secure SECRET_KEY for Django"""
        if options.get("django_key", False):
            key = ''.join(random.SystemRandom().choice(
                'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)')
                for _ in range(50))
        else:
            key = None
        return key

    def _print_verbose(self, min_verbose_lvl, msg):
        """Prints message according to the current verbosity level.

        Prints the 'msg' only if the verbosity level chosen by the user is
        greater or equal than 'min_verbose_lvl'.
        """
        if self.verbose >= min_verbose_lvl:
            print(msg)

    def _read_credentials(self, env_path):
        """Look for CUSTOMER_ID and CUSTOMER_KEY only in the specified file.

        By default django-environ looks for variables in settings/.env, even
        if a custom .env file is specified. In order to avoid mixing up the
        credentials, we force-read them from the custom .env file.
        """
        customer_id = customer_key = None
        with open(env_path, 'r') as f_env:
            for line in f_env.readlines():
                if line.startswith('CUSTOMER_ID='):
                    customer_id = line.replace('CUSTOMER_ID=', "").replace("\n", "").strip()
                elif line.startswith('CUSTOMER_KEY='):
                    customer_key = line.replace('CUSTOMER_KEY=', "").replace("\n", "").strip()
        return customer_id, customer_key
