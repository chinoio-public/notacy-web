# Copyright (c) 2020 Chino.io
import django.contrib.auth.forms as authforms
import django.forms as djforms
from chino.exceptions import CallError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button, Fieldset, Div
from django import urls
from django.conf import settings
from django.core.exceptions import ValidationError

from notes.backends import ChinoAuthBackend
from notes.utils import LoginTokenManager, AuthUtils


# FIELDS

class TagCheckBoxes(djforms.CheckboxSelectMultiple):
    def set_tag_list(self, ls: list):
        self.choices = ls


class TagSelector(djforms.MultipleChoiceField):
    widget = TagCheckBoxes()

    def get_choices(self, ls: list):
        self.widget.set_tag_list(ls)


# FORM

class BaseCrispy(djforms.Form):
    """
    Provides basic configuration to use Django forms with Crispy
    """
    field_css_class = ""

    def __init__(self, wrap=False, *args, **kwargs):
        """
        initialize form and the Crispy FormHelper for this object.

        :param wrap: if `True`, the form will be wrapped by &lt;form&gt;&lt;/form&gt; tags. Default: `False`.
        """
        # A: TODO: This line doesn't seem to be useful. Check if it is required and eventually remove it, or explain.
        # kwargs.pop('request', None)
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = wrap

    def css_class(self, css_class: str):
        """
        Set the CSS class of this form.

        This method exists to have consistent naming with the `css_class` attribute of Crispy buttons.
        Only works if the form was created with `wrap=True`.

        :param css_class: the CSS class for this form's &lt;form&gt;&lt;/form&gt; tags
        """
        self.helper.form_class = css_class


class NoteForm(BaseCrispy):
    """
    Form that is used for creation and editing of Notes.
    """
    title = djforms.CharField(label="Title", widget=djforms.TextInput(attrs={'placeholder': 'Note title'}),
                              max_length=32, min_length=1)
    content = djforms.CharField(label="Content",
                                widget=djforms.Textarea(attrs={'placeholder': 'Write something down...'}),
                                required=False)
    tags = djforms.MultipleChoiceField(label="Tags", widget=djforms.CheckboxSelectMultiple(), required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.add_layout(Fieldset(
            '',
            Div(
                'title',
                'content',
                css_class="no-labels"
            ),
            Div(
                'tags',
                css_id="taglist",
                css_class="label-txt-visible"
            )
        ))

    def to_json(self):
        """
        Clean this form's data so that they can be passed to Chino.io Documents API

        :return: a dict that can be passed as 'content' to the Documents create() and update() methods
        """
        data = self.clean()
        data["content"] = str(data["content"]).replace("\r\n", "\n")
        data["title"] = str(data["title"]).replace("\r\n", "").replace("\n", " ")
        return data


class LoginForm(BaseCrispy):
    """
    Form that handles the login operation of Notacy users.
    """
    username = djforms.EmailField(label="username", widget=djforms.EmailInput)
    password = djforms.CharField(label="password", widget=djforms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super().__init__(wrap=True, *args, **kwargs)
        self.css_class('form-signin')
        # Login
        self.helper.add_input(
            Submit("login-btn", "Login", css_class="btn btn-success")
        )
        # Password reset
        reset_psw_url = urls.reverse('password_reset')
        self.helper.add_input(
            Button("psw-reset", "Forgot your password?", type="button", css_class="btn btn-warning",
                   onclick='window.location.assign("' + reset_psw_url + '");')
        )


class RegisterForm(BaseCrispy):
    """
    Form that handles the registration of new Notacy users.
    """
    first_name = djforms.CharField(label="First Name")
    last_name = djforms.CharField(label="Last Name")
    username = djforms.EmailField(label="Username (your email address)", widget=djforms.EmailInput)
    password = djforms.CharField(label="Choose a password (length >= 8)", widget=djforms.PasswordInput, min_length=8)
    repeat_password = djforms.CharField(label="Repeat password", widget=djforms.PasswordInput, min_length=8)

    def __init__(self, *args, **kwargs):
        super().__init__(wrap=True, *args, **kwargs)
        self.css_class('form-signin')
        # Submit
        self.helper.add_input(
            Submit("confirm-register", "Create account", css_class="btn btn-lg btn-primary btn-block")
        )

    def clean_repeat_password(self):
        """
        Check that passwords match.

        :return: the valid password
        """
        psw = self.cleaned_data["password"]
        rep = self.cleaned_data["repeat_password"]
        if psw != rep:
            raise djforms.ValidationError("Passwords do not match!")
        else:
            return rep


# Password recovery
class NotacyPasswordResetForm(authforms.PasswordResetForm, BaseCrispy):
    """
    Config django authforms for requesting password recovery
    """

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(wrap=True, *args, **kwargs)
        self.helper.add_input(
            Submit('reset_psw', 'Send email', css_class="btn-success")
        )

    # overrides PasswordResetForm.get_users
    def get_users(self, email):
        chino = AuthUtils.get_client(auth="customer")
        # search if provided email matches a registered user's username
        result = chino.searches.users(
            settings.USER_SCHEMA_ID,
            result_type="FULL_CONTENT",
            filters=[{
                "field": "username",
                "type": "eq",
                "value": email
            }]
        )
        # add information to the user so that Django system can generate a psw reset token
        return [LoginTokenManager._wrap_user_for_token(user) for user in result.users]


class NotacyPasswordSetForm(authforms.SetPasswordForm, BaseCrispy):
    """
    Config django authforms for setting a new password
    """
    btn_text = 'Change password'

    def __init__(self, *args, **kwargs):
        super().__init__(wrap=True, *args, **kwargs)
        # Submit btn
        self.helper.add_input(
            Submit('save_psw', self.btn_text, css_class="btn-success")
        )

    def save(self, commit=True):
        """
        Save the new password to chino.io

        :param commit: ignored
        :return: the user with the updated password
        """
        password = self.cleaned_data["new_password1"]
        self.user.password = password
        # update password on Chino.io
        AuthUtils.get_client(auth="customer") \
            .users.partial_update(self.user.user_id,
                                  consistent=True,
                                  password=password)
        return self.user


class NotacyPasswordChangeForm(authforms.PasswordChangeForm, NotacyPasswordSetForm):
    """
    Config django authforms for changing password (once User is logged in)
    """

    def clean_old_password(self):
        """
        Validate the `old_password` field. Check against Chino.io that the user can authenticate with the old password.

        :return: the old password, cleaned
        """
        old_pw = self.cleaned_data["old_password"]
        try:
            # Verify that inserted password is valid
            self.user = ChinoAuthBackend().authenticate(username=self.user.username, password=old_pw)
        except CallError:
            # password not valid, return error on field
            raise ValidationError("Insert your old password")
        return old_pw

    def save(self, commit=True):
        """
        Save the new password to chino.io

        :param commit: ignored
        :return: the user with the updated password
        """
        password = self.cleaned_data["new_password1"]
        # update password on Chino.io
        AuthUtils.get_client(auth="customer") \
            .users.partial_update(self.user.user_id,
                                  consistent=True,
                                  password=password)
        return self.user
