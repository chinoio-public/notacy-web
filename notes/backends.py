# Copyright (c) 2020 Chino.io
import logging

from chino.api import ChinoAPIClient
from chino.exceptions import CallError
from django.conf import settings

log = logging.getLogger("notes.backends")


class ChinoAuthBackend:
    """
    Django backend that handles all authentication operations
    """

    # default Chino.io client
    chino = ChinoAPIClient(customer_id=settings.CHINO["CUSTOMER_ID"],
                           url=settings.CHINO["HOST"],
                           client_id=settings.CHINO["APP_ID"],
                           client_secret=settings.CHINO["APP_SECRET"])

    def authenticate(self, username=None, password=None,
                     tokens=None, access_token=None, refresh_token=None):
        """
        This method will try to login with the provided credentials and return the authenticated user.
        Authenticated methods will be considered in this order:
            -   if `access_token` AND `refresh_token` are defined, they will be used as auth method
            -   if `tokens` is a (dict) and contains `access_token` AND `refresh_token`, it will be used as auth method
            -   if `username` AND `password` are defined, they will be used as auth method with the APP_ID
                specified from the environment
            -   if no parameters are passed, the authentication will fail.
        In case of success, the returned user will contain:
            -   `user.is_authenticated`: True, meant to be used within Django's own auth system
            -   `user.tokens`: a (dict) containing the user's access tokens

        :param username: [optional] the username
        :param password: [optional] the password
        :param tokens: [optional] a (dict) containing "access_token" and "refresh_token"
        :param access_token: [optional] a bearer token from an authenticated user
        :param refresh_token: [optional] a refresh token from an authenticated user

        :return: the authenticated user, or None if the credentials are invalid
        """
        if access_token and refresh_token:
            tokens = dict(access_token=access_token, refresh_token=refresh_token)

        user = None
        if tokens:
            user = self._verify_token(tokens)
        elif username and password:
            user = self._verify_credentials(username, password)

        if user:
            setattr(user, "is_authenticated", True)
            setattr(user, "tokens", dict(
                access_token=self.chino.auth.bearer_token,
                refresh_token=self.chino.auth.refresh_token,
                expiry=self.chino.auth.bearer_exp))

        return user

    def _verify_token(self, tokens):
        """
        Store the tokens in the chino api client and attempt login with them

        :param tokens: a (dict) containing keys "access_token" and "refresh_token"

        :return: the authenticated user, or None if the credentials are invalid
        """
        try:
            self.chino.auth.bearer_token = tokens["access_token"]
            self.chino.auth.refresh_token = tokens["refresh_token"]
            self.chino.auth.set_auth_user()
            return self.chino.users.current()
        except CallError as e:
            log.debug(f'Login with token failed: {e.code}, {e.message}')
            return None

    def _verify_credentials(self, username, password):
        """
        Verify username and password

        :param username: (str)
        :param password: (str)

        :return: the authenticated user, or None if the credentials are invalid
        """
        try:
            self.chino.users.login(username, password)
            return self.chino.users.current()
        except CallError as e:
            log.debug(f'Login failed for username "{username}": {e.code}, {e.message}')
            return None
