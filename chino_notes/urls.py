import django.contrib.auth.views as auth
from django.conf import settings
from django.urls import path
from django.conf.urls.static import static

from notes import views

"""
chino_notes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

app_name = 'notes'

urlpatterns = [
    # Auth
    path('', views.LoginView.as_view(), name='login'),
    path('register', views.RegisterView.as_view(), name='register'),
    path('logout', views.LogoutView.as_view(), name='logout'),

    # Notes
    path('notes', views.DocumentListView.as_view(), name='dashboard'),
    path('notes/new', views.EditNoteView.as_view(), name='create'),
    path('notes/<uuid:document_id>', views.EditNoteView.as_view(), name='edit'),
    path('notes/delete', views.DeleteNoteView.as_view(), name='delete'),

    # User
    path('profile/', views.UserDetailsView.as_view(), name='user'),
    path('profile/edit', views.UserUpdateView.as_view(), name='user_edit'),
    path('profile/edit/done', auth.PasswordChangeDoneView.as_view(), name='user_edit_done'),
    path('profile/delete', views.UserDeleteView.as_view(), name='user_delete'),

    # Password reset workflow
    path('reset', views.NotacyPasswordResetView.as_view(), name='password_reset'),
    path('reset/done', views.NotacyPasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>', views.NotacyPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/success', views.NotacyPasswordResetCompleteView.as_view(), name='password_reset_complete'),

    # Tags
    path('tags', views.ManageTagsView.as_view(), name='tags'),
    path('tags/delete', views.DeleteTagView.as_view(), name='tags_delete'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
